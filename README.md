## VỀ FONT CHỮ 
Font chữ cho body: SVN-GilroyMedium, SVN-GilroyBold cho thẻ &lt;strong&gt;.

Font chữ cho Heading (H1 - H6): UTM-Avo-Bold.

(Tất cả font sẽ có trong channel Slack của dự án, các bạn vào đó lấy nếu cần)

##  VỀ CÁCH VIẾT HTML 

1/ Đối với Front-end sẽ làm trực tiếp trên máy tính rồi push lên Gitlab cá nhân, anh sẽ pull về xử lý sau.

2/ Khi làm thì sẽ làm trên page modules.html, có thẻ &lt;span&gt; Tiêu đề của section &lt;/span&gt; rồi bên dưới là module chứa trong cặp thẻ &lt;section&gt; ... &lt;/section&gt;.

3/ Khi đặt class-name cho section, phải đặt theo cấu trúc “w-tênSection” (Có prefix w- đầu tiên)

4/ Đối với những section full width sẽ có thêm class-name “align-full”, với những section bình thường thì sẽ có class-name là “align-wide”

5/ CSS và jQuery sẽ viết bên dưới section luôn chứ không viết vào file css hoặc file js. 
